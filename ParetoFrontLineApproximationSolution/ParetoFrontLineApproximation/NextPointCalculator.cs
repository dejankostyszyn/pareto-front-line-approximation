﻿using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Step 5 + 6.
    /// </summary>
    public class NextPointCalculator
    {
        private readonly NormalVectorCalculator _normalVectorCalculator;
        private readonly NovelVertexCalculator _novelVertexCalculator;
        private readonly VertexWeightsRepository _vertexWeightsRepository;

        public NextPointCalculator(NormalVectorCalculator normalVectorCalculator, NovelVertexCalculator novelVertexCalculator, VertexWeightsRepository vertexWeightsRepository)
        {
            _normalVectorCalculator = normalVectorCalculator;
            _novelVertexCalculator = novelVertexCalculator;
            _vertexWeightsRepository = vertexWeightsRepository;
        }

        public Point2D? Calculate(LowerDistalPointResult lowerDistalPointResult)
        {
            var lineSegment = lowerDistalPointResult.AccordingLineSegment;
            var weightVector = _normalVectorCalculator.CalculateFirstNormalVector(lineSegment);
            weightVector = weightVector.Normalize();

            if (_vertexWeightsRepository.HasWeightBeenUsed(weightVector)) return null;
            var lowerDistalPoint =
                _novelVertexCalculator.CalculateNovelPointViaNonlinearConstrainedOptimization(weightVector);
            _vertexWeightsRepository.MarkWeightAsUsed(weightVector);
            _vertexWeightsRepository.MarkLineSegmentAsUsed(lineSegment);
            return lowerDistalPoint;
        }
    }
}
