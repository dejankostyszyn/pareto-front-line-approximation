﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class NormalVectorCalculator
    {
        public Vector2D CalculateSecondNormalVector(LineSegment2D lineSegment)
        {
            var (dx, dy) = CalculateDistances(lineSegment);
            return new Vector2D(dy, -dx);
        }

        public Vector2D CalculateFirstNormalVector(LineSegment2D lineSegment)
        {
            var (dx, dy) = CalculateDistances(lineSegment);
            return new Vector2D(-dy, dx);
        }

        private (double, double) CalculateDistances(LineSegment2D lineSegment)
        {
            var dx = lineSegment.EndPoint.X - lineSegment.StartPoint.X;
            var dy = lineSegment.EndPoint.Y - lineSegment.StartPoint.Y;
            return (dx, dy);
        }
    }
}
