﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    public class BoundingBoxCalculator
    {
        private readonly HyperplaneBuilder _hyperplaneBuilder;
        private readonly VertexWeightsRepository _vertexWeightsRepository;

        public BoundingBoxCalculator(HyperplaneBuilder hyperplaneBuilder, VertexWeightsRepository vertexWeightsRepository)
        {
            _hyperplaneBuilder = hyperplaneBuilder;
            _vertexWeightsRepository = vertexWeightsRepository;
        }

        public BoundingBox Calculate(Point2D[] points, UpperParetoSurface upperParetoSurface, AnchorAndInteriorPointsResult anchorAndInteriorPoints)
        {
            ValidateInput(points, upperParetoSurface, anchorAndInteriorPoints);
            var hyperplanes = CalculateHyperplanes(upperParetoSurface.Vertices);

            var pointsWithoutAnchorPoints = points.ToList();
            pointsWithoutAnchorPoints.Remove(anchorAndInteriorPoints.XAnchorPoint);
            pointsWithoutAnchorPoints.Remove(anchorAndInteriorPoints.YAnchorPoint);
            
            var upperXBound = pointsWithoutAnchorPoints.OrderByDescending(x => x.X).First().X;
            var upperYBound = pointsWithoutAnchorPoints.OrderByDescending(x => x.Y).First().Y;
            return new BoundingBox(hyperplanes, upperXBound, upperYBound);
        }

        private List<Hyperplane> CalculateHyperplanes(Point2D[] vertices)
        {
            var hyperplanes = new List<Hyperplane>();
            foreach (var vertex in vertices)
            {
                var normalVector = _vertexWeightsRepository.GetWeightsOfPointOrDefault(vertex);
                if (!normalVector.HasValue)
                    throw new ArgumentException($"Vertex weights of vertex {vertex} must already be set.");
                hyperplanes.Add(_hyperplaneBuilder.Build(normalVector.Value, vertex));
            }

            return hyperplanes;
        }

        private void ValidateInput(Point2D[] points, UpperParetoSurface upperParetoSurface, AnchorAndInteriorPointsResult anchorAndInteriorPoints)
        {
            if (points == null) throw new ArgumentNullException(nameof(points));
            if (upperParetoSurface == null) throw new ArgumentNullException(nameof(upperParetoSurface));
            if (anchorAndInteriorPoints == null) throw new ArgumentNullException(nameof(anchorAndInteriorPoints));
            if (!upperParetoSurface.Facets.Any())
                throw new ArgumentException("Cannot calculate bounding box of empty set of line segments.");
        }
    }
}
