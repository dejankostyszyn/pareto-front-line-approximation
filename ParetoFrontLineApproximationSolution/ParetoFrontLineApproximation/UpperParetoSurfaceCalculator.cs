﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Step 2.2.
    /// </summary>
    public class UpperParetoSurfaceCalculator
    {
        private readonly NormalVectorCalculator _normalVectorCalculator;

        public UpperParetoSurfaceCalculator(NormalVectorCalculator normalVectorCalculator)
        {
            _normalVectorCalculator = normalVectorCalculator;
        }

        public UpperParetoSurface Calculate(IEnumerable<LineSegment2D> lineSegments)
        {
            if (!lineSegments.Any())
                throw new ArgumentException("At least one line segment must be given. Zero were given.");
            var upperParetoSurfaceLineSegments = RemoveLineSegmentsWithAllNegativeNormals(lineSegments);
            var orderedUpperParetoSurfaceLineSegments = OrderLineSegments(upperParetoSurfaceLineSegments);
            var upperParetoSurfacePoints = ExtractPointsFromLineSegments(orderedUpperParetoSurfaceLineSegments);
            return new UpperParetoSurface(upperParetoSurfacePoints, orderedUpperParetoSurfaceLineSegments);
        }

        private LineSegment2D[] OrderLineSegments(IEnumerable<LineSegment2D> lineSegments)
        {
            return lineSegments.OrderBy(x => x.StartPoint.X).ToArray();
        }

        private Point2D[] ExtractPointsFromLineSegments(IEnumerable<LineSegment2D> lineSegments)
        {
            var points = new HashSet<Point2D>();
            foreach (var lineSegment in lineSegments)
            {
                points.Add(lineSegment.StartPoint);
                points.Add(lineSegment.EndPoint);
            }

            return points.ToArray();
        }

        private List<LineSegment2D> RemoveLineSegmentsWithAllNegativeNormals(IEnumerable<LineSegment2D> lineSegments)
        {
            var relevantLineSegments = new List<LineSegment2D>(lineSegments);
            foreach (var lineSegment in lineSegments)
            {
                var normal = _normalVectorCalculator.CalculateFirstNormalVector(lineSegment);
                if (normal.X < 0 || normal.Y < 0) relevantLineSegments.Remove(lineSegment);
            }

            return relevantLineSegments;
        }
    }
}
