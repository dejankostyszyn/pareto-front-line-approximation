﻿using Accord.Math.Optimization;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class NovelVertexCalculator
    {
        public uint MaximumEvaluationsOfNonlinearOptimizer { get; }

        /// <summary>
        /// A class for calculating novel vertices with Conjugated Gradient
        /// and Augmented Lagrangian methods.
        /// </summary>
        /// <param name="maximumEvaluationsOfNonlinearOptimizer">
        /// Maximum number of evaluation steps to avoid too long calculation
        /// times. 0 means optimization until convergence.
        /// </param>
        public NovelVertexCalculator(uint maximumEvaluationsOfNonlinearOptimizer = 5000)
        {
            MaximumEvaluationsOfNonlinearOptimizer = maximumEvaluationsOfNonlinearOptimizer;
        }

        public Point2D CalculateNovelPointViaNonlinearConstrainedOptimization(Vector2D weightVector)
        {

            var w1 = weightVector.X;
            var w2 = weightVector.Y;

            var function = new NonlinearObjectiveFunction(2,
                function: x => w1 * x[0] + w2 * x[1],
                gradient: x => new[] { w1, w2 });

            NonlinearConstraint[] constraints =
            {
                new NonlinearConstraint(function,
                    function: x => x[0],
                    shouldBe: ConstraintType.GreaterThanOrEqualTo,
                    value: 0,
                    gradient: x => new[] {1.0, 1.0}),
                new NonlinearConstraint(function,
                    function: x => x[1],
                    shouldBe: ConstraintType.GreaterThanOrEqualTo,
                    value: 0,
                    gradient: x => new[] {1.0, 1.0}),
            };

            var target = new ConjugateGradient(2);
            var solver = new AugmentedLagrangian(target, function, constraints);
            solver.MaxEvaluations = (int)MaximumEvaluationsOfNonlinearOptimizer;
            var success = solver.Minimize();
            var result = solver.Solution;

            if (result[0] < 0) result[0] = 0.0;
            if (result[1] < 0) result[1] = 0.0;

            return new Point2D(result[0], result[1]);
        }
    }
}
