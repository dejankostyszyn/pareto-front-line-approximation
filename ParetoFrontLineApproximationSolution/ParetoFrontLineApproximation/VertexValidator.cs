﻿using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    public class VertexValidator
    {
        public bool IsVertexLocatedWithinBoundingBox(Point2D vertex, BoundingBox boundingBox)
        {
            if (vertex.X > boundingBox.UpperXBound || vertex.Y > boundingBox.UpperYBound) return false;
            foreach (var hyperplane in boundingBox.LowerBound)
            {
                if (!IsVertexLocatedAboveLowerBoundHyperplane(vertex, hyperplane)) return false;
            }

            return true;
        }

        private bool IsVertexLocatedAboveLowerBoundHyperplane(Point2D vertex, Hyperplane hyperplane)
        {
            var result = hyperplane.NormalVector.DotProduct(vertex - hyperplane.PointOnPlane);
            return result > 0.0;
        }
    }
}
