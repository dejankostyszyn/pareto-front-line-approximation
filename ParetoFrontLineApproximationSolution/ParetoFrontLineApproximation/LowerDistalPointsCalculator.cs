﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Step 3.
    /// </summary>
    public class LowerDistalPointsCalculator
    {
        private readonly NormalVectorCalculator _normalVectorCalculator;
        private readonly HyperplaneBuilder _hyperplaneBuiler;
        private readonly VertexValidator _vertexValidator;

        public LowerDistalPointsCalculator(NormalVectorCalculator normalVectorCalculator, HyperplaneBuilder hyperplaneBuiler, VertexValidator vertexValidator)
        {
            _normalVectorCalculator = normalVectorCalculator;
            _hyperplaneBuiler = hyperplaneBuiler;
            _vertexValidator = vertexValidator;
        }

        public LowerDistalPoints Calculate(VertexWeightsRepository vertexWeightsRepository, LineSegment2D[] lineSegments, BoundingBox boundingBox)
        {
            var lowerDistalPointResults = new List<LowerDistalPointResult>();
            for (var i = 0; i < lineSegments.Length - 1; i++)
            {
                var leftLineSegment = lineSegments[i];
                var rightLineSegment = lineSegments[i + 1];
                if (leftLineSegment.EndPoint != rightLineSegment.StartPoint)
                    throw new ArgumentException("Line segments are not ordered ascending wrt. X.");
                var lowerDistalPoint = CalculateLowerDistalPoint(leftLineSegment, vertexWeightsRepository);
                if (_vertexValidator.IsVertexLocatedWithinBoundingBox(lowerDistalPoint.LowerDistalPoint, boundingBox))
                    lowerDistalPointResults.Add(lowerDistalPoint);
            }

            var lastLineSegment = lineSegments.Last();
            lowerDistalPointResults.Add(CalculateLowerDistalPoint(lastLineSegment, vertexWeightsRepository));

            return new LowerDistalPoints(lowerDistalPointResults.ToArray());
        }
        

        private LowerDistalPointResult CalculateLowerDistalPoint(LineSegment2D lineSegment, VertexWeightsRepository pointWeightsRepository)
        {
            var leftPoint = lineSegment.StartPoint;
            var leftNormal = pointWeightsRepository.GetWeightsOfPointOrDefault(leftPoint);

            var rightPoint = lineSegment.EndPoint;
            var rightNormal = pointWeightsRepository.GetWeightsOfPointOrDefault(rightPoint);

            if (leftNormal == null || rightNormal == null)
                throw new ArgumentException(
                    $"Weights of line segment vertices must already be defined, but were {leftNormal} and {rightNormal} for line segment {lineSegment}.");

            var lowerDistalPointResult = SolveEquation(lineSegment, leftNormal.Value, rightNormal.Value);
            return lowerDistalPointResult;
        }

        private LowerDistalPointResult SolveEquation(LineSegment2D lineSegment, Vector2D leftNormal, Vector2D rightNormal)
        {
            var leftPoint = lineSegment.StartPoint;
            var rightPoint = lineSegment.EndPoint;

            var A = Matrix<double>.Build.DenseOfArray(new double[,]
            {
            {leftNormal.X,  leftNormal.Y},
            {rightNormal.X,  rightNormal.Y},
            });

            var b1 = leftNormal.X * leftPoint.X + leftNormal.Y * leftPoint.Y;
            var b2 = rightNormal.X * rightPoint.X + rightNormal.Y * rightPoint.Y;
            var b = Vector<double>.Build.Dense(new double[]
            {
            b1, b2
            });
            var result = A.Solve(b).ToArray();
            var lowerDistalPoint = new Point2D(result[0], result[1]);
            var lowerDistalPointNormal = _normalVectorCalculator.CalculateFirstNormalVector(lineSegment);
            var accordinghyperplane = _hyperplaneBuiler.Build(lowerDistalPointNormal, leftPoint);
            return new LowerDistalPointResult(lowerDistalPoint, accordinghyperplane, lineSegment);

        }

        private Vector2D CalculateWeightVector(LineSegment2D leftLineSegment, LineSegment2D rightLineSegment)
        {
            var leftNormal = _normalVectorCalculator.CalculateFirstNormalVector(leftLineSegment);
            var rightNormal = _normalVectorCalculator.CalculateFirstNormalVector(rightLineSegment);
            var x = (leftNormal.X + rightNormal.X) / 2.0;
            var y = (leftNormal.Y + rightNormal.Y) / 2.0;
            return new Vector2D(x, y);
        }
    }
}
