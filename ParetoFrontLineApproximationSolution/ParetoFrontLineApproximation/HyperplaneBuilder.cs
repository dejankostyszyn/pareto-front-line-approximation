﻿using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    public class HyperplaneBuilder
    {
        public Hyperplane Build(Vector2D normalVector, Point2D pointOnHyperplane)
        {
            var n1 = normalVector.X;
            var n2 = normalVector.Y;
            var c = n1 * pointOnHyperplane.X + n2 * pointOnHyperplane.Y;
            return new Hyperplane(normalVector, pointOnHyperplane, c);
        }
    }
}
