﻿using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    public class AdditionalSolutionsCalculator
    {
        private readonly LineSegmentsFromPointsCreator _lineSegmentsFromPointsCreator;

        public AdditionalSolutionsCalculator(LineSegmentsFromPointsCreator lineSegmentsFromPointsCreator)
        {
            _lineSegmentsFromPointsCreator = lineSegmentsFromPointsCreator;
        }

        public ParetoFrontLine CalculateAndAddAdditionalSolutions(ParetoFrontLine paretoFrontLine, double lengthBetweenPoints = 0.01)
        {
            while (true)
            {
                var lineSegments =
                    _lineSegmentsFromPointsCreator.CreateFromOrderedDescendingWrtX(paretoFrontLine.Vertices);
                var lineSegmentsWithLengthInScope = lineSegments.Where(x => x.Length >= lengthBetweenPoints);
                if (!lineSegmentsWithLengthInScope.Any())
                    return paretoFrontLine;

                var weightedAverageVectors = new List<Vector2D>();
                foreach (var lineSegment in lineSegmentsWithLengthInScope)
                    weightedAverageVectors.Add(CalulateWeightedAverage(lineSegment));

                var points = weightedAverageVectors.Select(x => new Point2D(x.X, x.Y));
                var concatenatedPoints =
                    paretoFrontLine.Vertices.Concat(points).OrderBy(x => x.X).ToArray();
                paretoFrontLine = new ParetoFrontLine(concatenatedPoints);
            }
        }

        private Vector2D CalulateWeightedAverage(LineSegment2D lineSegment)
        {
            var startPoint = lineSegment.StartPoint.ToVector2D();
            var endPoint = lineSegment.EndPoint.ToVector2D();
            return 0.5 * (startPoint + endPoint);
        }
    }
}
