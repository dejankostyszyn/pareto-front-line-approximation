﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using MIConvexHull;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Setp 2.1.
    /// </summary>
    public class ConvexHullCalculator
    {
        private readonly LineSegmentsFromPointsCreator _lineSegmentsFromPointsCreator;

        public ConvexHullCalculator(LineSegmentsFromPointsCreator lineSegmentsFromPointsCreator)
        {
            _lineSegmentsFromPointsCreator = lineSegmentsFromPointsCreator;
        }

        public ConvexHullResult Calculate(IEnumerable<Point2D> points)
        {
            if (points.Count() < 2)
                throw new ArgumentException(
                    $"At least 2 vertices must be given to calculate convex hull, but {points.Count()} were given.");

            var convertedPoints = ConvertPoints2DToIVertices(points);
            var convexHull = ConvexHull.Create2D(convertedPoints).Result;
            var convexHullPoints = ConvertVertex2DsToPoint2Ds(convexHull);
            var convexHullLineSegments = _lineSegmentsFromPointsCreator.CreateFromConvexHul(convexHullPoints);
            return new ConvexHullResult(convexHullPoints, convexHullLineSegments);
        }

        private Point2D[] ConvertVertex2DsToPoint2Ds(IList<DefaultVertex2D> convexHull)
        {
            var result = new Point2D[convexHull.Count];
            for (var i = 0; i < convexHull.Count; i++)
            {
                var vertex2D = convexHull[i];
                result[i] = new Point2D(vertex2D.X, vertex2D.Y);
            }

            return result;
        }

        private List<DefaultVertex2D> ConvertPoints2DToIVertices(IEnumerable<Point2D> points)
        {
            return points.Select(point2D => new DefaultVertex2D(point2D.X, point2D.Y)).ToList();
        }
    }
}
