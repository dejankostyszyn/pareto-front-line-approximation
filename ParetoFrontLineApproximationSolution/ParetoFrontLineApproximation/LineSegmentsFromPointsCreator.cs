﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class LineSegmentsFromPointsCreator
    {
        public LineSegment2D[] CreateFromConvexHul(Point2D[] points)
        {
            var lineSegments = new List<LineSegment2D>();
            lineSegments.Add(new LineSegment2D(points.Last(), points.First()));

            for (int i = 1; i < points.Length; i++)
            {
                var startPoint = points[i - 1];
                var endpoint = points[i];
                lineSegments.Add(new LineSegment2D(startPoint, endpoint));
            }

            return lineSegments.ToArray();
        }

        public LineSegment2D[] CreateFromOrderedDescendingWrtX(Point2D[] points)
        {
            var lineSegments = new List<LineSegment2D>();
            var tolerance = 0.0001;
            for (var i = 0; i < points.Length-1; i++)
            {
                var leftPoint = points[i];
                var rightPoint = points[i + 1];

                if (Math.Abs(leftPoint.X - rightPoint.X) > tolerance &&
                    Math.Abs(leftPoint.Y - rightPoint.Y) > tolerance)
                        lineSegments.Add(new LineSegment2D(leftPoint, rightPoint));
            }

            return lineSegments.ToArray();
        }
    }
}
