﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    public class ParetoFrontLineApproximator
    {
        private readonly AnchorPointsAndInteriorPointCalculator _anchorPointAndInteriorPointCalculator;
        private readonly ConvexHullCalculator _convexHullCalculator;
        private readonly UpperParetoSurfaceCalculator _upperParetoSurfaceCalculator;
        private readonly VertexWeightsManager _vertexWeightsManager;
        private readonly LowerDistalPointsCalculator _lowerDistalPointsCalculator;
        private readonly FacetSelector _facetSelector;
        private readonly NextPointCalculator _nextPointCalculator;
        private readonly BoundingBoxCalculator _boundingBoxCalculator;
        private readonly VertexValidator _vertexValidator;
        private readonly AdditionalSolutionsCalculator _additionalSolutionsCalculator;
        private readonly VertexWeightsRepository _vertexWeightsRepository;

        public ParetoFrontLineApproximator(
            AnchorPointsAndInteriorPointCalculator anchorPointAndInteriorPointCalculator,
            ConvexHullCalculator convexHullCalculator,
            UpperParetoSurfaceCalculator upperParetoSurfaceCalculator,
            VertexWeightsManager vertexWeightsManager,
            LowerDistalPointsCalculator lowerDistalPointsCalculator,
            FacetSelector facetSelector,
            NextPointCalculator nextPointCalculator,
            BoundingBoxCalculator boundingBoxCalculator,
            VertexValidator vertexValidator,
            AdditionalSolutionsCalculator additionalSolutionsCalculator,
            VertexWeightsRepository vertexWeightsRepository
        )
        {
            _anchorPointAndInteriorPointCalculator = anchorPointAndInteriorPointCalculator;
            _convexHullCalculator = convexHullCalculator;
            _upperParetoSurfaceCalculator = upperParetoSurfaceCalculator;
            _vertexWeightsManager = vertexWeightsManager;
            _lowerDistalPointsCalculator = lowerDistalPointsCalculator;
            _facetSelector = facetSelector;
            _nextPointCalculator = nextPointCalculator;
            _boundingBoxCalculator = boundingBoxCalculator;
            _vertexValidator = vertexValidator;
            _additionalSolutionsCalculator = additionalSolutionsCalculator;
            _vertexWeightsRepository = vertexWeightsRepository;
        }

        /// <summary>
        /// Approximate the 2D pareto front line, given 2D points.
        /// </summary>
        /// <param name="points">2D input points, e.g. experimental measurements.</param>
        /// <param name="maximumAcceptableDistanceOfLowerDistalPoints">The algorithm requires to set a limit, above which calculated lower distal points have to be rejected. For further information, read the proposed algorithm.</param>
        /// <param name="maximumIterations">Maximum iterations after which the program shall terminate, whether or not it has converged.</param>
        /// <param name="maximumDistanceBetweenAdditionalSolutions">After the actual algorithm, additional solution points are generated. With this parameter the maximal distance between the pareto front line points can be set.</param>
        /// <returns>A set of points, sorted ascending wrt. x-coordinate. These points form the approximated 2D pareto front line.</returns>
        public ParetoFrontLine Approximate(IEnumerable<Point2D> points, double maximumAcceptableDistanceOfLowerDistalPoints = 0.01, int maximumIterations = int.MaxValue, double maximumDistanceBetweenAdditionalSolutions = 0.01)
        {
            int iteration = 0;
            _vertexWeightsRepository.ClearAll();
            ParetoFrontLine paretoFrontLine;
            var anchorAndInteriorPoints = _anchorPointAndInteriorPointCalculator.Calculate(points);
            var allPoints = ConcatenatePoints(points, anchorAndInteriorPoints);
            var startingPoints = new Point2D[allPoints.Length];
            allPoints.CopyTo(startingPoints, 0);
            while (true)
            {
                var convexHull = _convexHullCalculator.Calculate(allPoints);
                var upperParetoSurface = _upperParetoSurfaceCalculator.Calculate(convexHull.Facets);
                _vertexWeightsManager.InitializeAllVertexWeights(_vertexWeightsRepository, upperParetoSurface,
                    anchorAndInteriorPoints);
                var boundingBox = _boundingBoxCalculator.Calculate(allPoints, upperParetoSurface, anchorAndInteriorPoints);
                var lowerDistalPoints =
                    _lowerDistalPointsCalculator.Calculate(_vertexWeightsRepository, upperParetoSurface.Facets, boundingBox);
                var lowerDistalPointWithHighestDistanceToItsLineSegment =
                    _facetSelector.GetLowerDistalPointWithHighestDistanceToItsLineSegment(lowerDistalPoints, maximumAcceptableDistanceOfLowerDistalPoints);
                if (lowerDistalPointWithHighestDistanceToItsLineSegment == null || iteration == maximumIterations)
                {
                    paretoFrontLine = new ParetoFrontLine(upperParetoSurface.Vertices); break;
                }
                var nextLowerDistalPoint =
                    _nextPointCalculator.Calculate(lowerDistalPointWithHighestDistanceToItsLineSegment);
                if (nextLowerDistalPoint != null) allPoints = AddNewPointToSetOfAllPoints(allPoints, nextLowerDistalPoint.Value, anchorAndInteriorPoints);
                iteration++;
            }

            return _additionalSolutionsCalculator.CalculateAndAddAdditionalSolutions(paretoFrontLine, maximumDistanceBetweenAdditionalSolutions);
        }

        private Point2D[] AddNewPointToSetOfAllPoints(Point2D[] points, Point2D pointToAdd, AnchorAndInteriorPointsResult anchorAndInteriorPoints)
        {
            var length = points.Length;
            var allPoints = new Point2D[length + 1];
            for (var i = 0; i < length; i++) allPoints[i] = points[i];
            allPoints[length] = pointToAdd;
            return allPoints;
        }

        private Point2D[] ConcatenatePoints(IEnumerable<Point2D> points, AnchorAndInteriorPointsResult anchorInteriorPointsResult)
        {
            var allPoints = new List<Point2D>(points);
            allPoints.Add(anchorInteriorPointsResult.InteriorPoint);
            allPoints.Add(anchorInteriorPointsResult.XAnchorPoint);
            allPoints.Add(anchorInteriorPointsResult.YAnchorPoint);
            return allPoints.ToArray();
        }
    }
}
