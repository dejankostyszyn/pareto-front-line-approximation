﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("ParetoFrontLineApproximation")]
[assembly: AssemblyDescription("A .Net Framework 4.7.2 implementation of the proposed algorithm for 2D Pareto front line approximation from the scientific paper: Craft et al. 2006. https://doi.org/10.1118/1.2335486")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Medical Center University of Freiburg im Breisgau")]
[assembly: AssemblyProduct("ParetoFrontLineApproximation")]
[assembly: AssemblyCopyright("Copyright © Dejan Kostyszyn 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly
// für COM-Komponenten unsichtbar.  Wenn Sie auf einen Typ in dieser Assembly von
// COM aus zugreifen müssen, sollten Sie das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("711ff62b-b6bf-46ed-8dd2-ffae9efee534")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// indem Sie "*" wie unten gezeigt eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.1")]
[assembly: AssemblyFileVersion("1.2.1")]
[assembly: NeutralResourcesLanguage("en")]
