﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Step 4.
    /// </summary>
    public class FacetSelector
    {
        private readonly VertexWeightsRepository _vertexWeightsRepository;

        public FacetSelector(VertexWeightsRepository vertexWeightsRepository)
        {
            _vertexWeightsRepository = vertexWeightsRepository;
        }

        public LowerDistalPointResult GetLowerDistalPointWithHighestDistanceToItsLineSegment(LowerDistalPoints lowerDistalPoints,
            double minimalAcceptableDistance)
        {
            if (minimalAcceptableDistance <= 0)
                throw new ArgumentException("Minimal acceptable distance cannot be negative.");

            var lowerDistalPointsWithTooLargeDistance = new List<LowerDistalPointResult>();
            foreach (var lowerDistalPointResult in lowerDistalPoints.LowerDistalPointResults)
            {
                var distance = lowerDistalPointResult.DistanceOfLowerDistalPointToAccordingLineSegment;
                if (distance > minimalAcceptableDistance) lowerDistalPointsWithTooLargeDistance.Add(lowerDistalPointResult);
            }

            var lowerDistalPointsOrderedDescending = lowerDistalPointsWithTooLargeDistance.OrderByDescending(x => x.DistanceOfLowerDistalPointToAccordingLineSegment);
            foreach (var lowerDistalPoint in lowerDistalPointsOrderedDescending)
                if (!_vertexWeightsRepository.HasLineSegmentBeenUsed(lowerDistalPoint.AccordingLineSegment))
                    return lowerDistalPoint;

            return null;
        }
    }
}
