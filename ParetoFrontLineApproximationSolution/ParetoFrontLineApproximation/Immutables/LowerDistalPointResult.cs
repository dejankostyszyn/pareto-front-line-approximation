﻿using System;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation.Immutables
{
    public class LowerDistalPointResult
    {
        public Point2D LowerDistalPoint { get; }
        public Hyperplane HyperplaneOfAccordingLineSegment { get; }
        public LineSegment2D AccordingLineSegment { get; }
        public double DistanceOfLowerDistalPointToAccordingLineSegment { get; }

        public LowerDistalPointResult(Point2D lowerDistalPoint, Hyperplane hyperplaneOfAccordingLineSegment, LineSegment2D accordingLineSegment)
        {
            LowerDistalPoint = lowerDistalPoint;
            HyperplaneOfAccordingLineSegment = hyperplaneOfAccordingLineSegment;
            AccordingLineSegment = accordingLineSegment;
            DistanceOfLowerDistalPointToAccordingLineSegment =
                CalculateDistanceOfLowerDistalPointToAccordingLineSegment();
        }
        private double CalculateDistanceOfLowerDistalPointToAccordingLineSegment()
        {
            var normalVector = HyperplaneOfAccordingLineSegment.NormalVector;
            var c = HyperplaneOfAccordingLineSegment.C;
            var euclideanNorm = normalVector.ToVector().L2Norm();
            var divisor = normalVector.DotProduct(LowerDistalPoint.ToVector2D()) - c;
            return Math.Abs(divisor / euclideanNorm);
        }
    }
}
