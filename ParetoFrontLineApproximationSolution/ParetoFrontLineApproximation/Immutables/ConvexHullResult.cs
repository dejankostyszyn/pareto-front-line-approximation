﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class ConvexHullResult
    {
        public Point2D[] Vertices { get; }
        public LineSegment2D[] Facets { get; }

        public ConvexHullResult(Point2D[] vertices, LineSegment2D[] facets)
        {
            Vertices = vertices;
            Facets = facets;
        }
    }
}
