﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation.Immutables
{
    public class ParetoFrontLine
    {
        public Point2D[] Vertices { get; }

        public ParetoFrontLine(Point2D[] vertices)
        {
            Vertices = vertices;
        }
    }
}
