﻿using System.Collections.Generic;

namespace ParetoFrontLineApproximation.Immutables
{
    public struct BoundingBox
    {
        public IEnumerable<Hyperplane> LowerBound { get; }
        public double UpperXBound { get; }
        public double UpperYBound { get; }

        public BoundingBox(IEnumerable<Hyperplane> lowerBound, double upperXBound, double upperYBound)
        {
            LowerBound = lowerBound;
            UpperXBound = upperXBound;
            UpperYBound = upperYBound;
        }
    }
}
