﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation.Immutables
{
    public struct Hyperplane
    {
        public Vector2D NormalVector { get; }
        public Point2D PointOnPlane { get; }
        public double C { get; }

        public Hyperplane(Vector2D normalVector, Point2D pointOnPlane, double c)
        {
            NormalVector = normalVector;
            PointOnPlane = pointOnPlane;
            C = c;
        }
    }
}
