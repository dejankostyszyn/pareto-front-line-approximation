﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class AnchorAndInteriorPointsResult
    {
        public Point2D XAnchorPoint { get; }
        public Point2D YAnchorPoint { get; }
        public Point2D InteriorPoint { get; }

        public AnchorAndInteriorPointsResult(Point2D xAnchorPoint, Point2D yAnchorPoint, Point2D interiorPoint)
        {
            XAnchorPoint = xAnchorPoint;
            YAnchorPoint = yAnchorPoint;
            InteriorPoint = interiorPoint;
        }
    }
}
