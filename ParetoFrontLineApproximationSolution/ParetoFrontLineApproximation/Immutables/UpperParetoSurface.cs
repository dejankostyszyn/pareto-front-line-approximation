﻿using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class UpperParetoSurface
    {
        public UpperParetoSurface(Point2D[] vertices, LineSegment2D[] facets)
        {
            Vertices = vertices;
            Facets = facets;
        }

        public Point2D[] Vertices { get; }
        public LineSegment2D[] Facets { get; }
    }
}
