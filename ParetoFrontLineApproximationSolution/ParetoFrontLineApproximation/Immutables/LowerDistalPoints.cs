﻿using System.Linq;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation.Immutables
{
    public class LowerDistalPoints
    {
        public LowerDistalPointResult[] LowerDistalPointResults { get; }

        public LowerDistalPoints(LowerDistalPointResult[] lowerDistalPointResults)
        {
            LowerDistalPointResults = lowerDistalPointResults;
        }

        public Point2D[] GetAllLowerDistalPoints() => LowerDistalPointResults.Select(x => x.LowerDistalPoint).ToArray();

        public Hyperplane[] GetAllAssociatedHyperPlanes() =>
            LowerDistalPointResults.Select(x => x.HyperplaneOfAccordingLineSegment).ToArray();
    }
}
