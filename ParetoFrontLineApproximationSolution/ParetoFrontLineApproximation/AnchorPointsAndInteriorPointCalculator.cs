﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    /// <summary>
    /// Step 1.
    /// </summary>
    public class AnchorPointsAndInteriorPointCalculator
    {
        private readonly NovelVertexCalculator _novelVertexCalculator;

        public AnchorPointsAndInteriorPointCalculator(NovelVertexCalculator novelVertexCalculator)
        {
            _novelVertexCalculator = novelVertexCalculator;
        }

        public AnchorAndInteriorPointsResult Calculate(IEnumerable<Point2D> points)
        {
            if (!points.Any())
                throw new ArgumentException(
                    "Cannot calculate anchor and interior points from empty argument enumerable");
            var (xAnchorPoint, yAnchorPoint) = CalculateAnchorPoints(points);
            var interiorPoint = CalculateInteriorPoint(xAnchorPoint, yAnchorPoint);
            return new AnchorAndInteriorPointsResult(xAnchorPoint, yAnchorPoint, interiorPoint);
        }

        private Point2D CalculateInteriorPoint(Point2D xAnchorPoint, Point2D yAnchorPoint)
        {
            var interiorPoint = new Vector2D(0.0, 0.0);
            var w = 1.0 / 2.0; // 1 / # points

            interiorPoint += xAnchorPoint.ToVector2D() * w;
            interiorPoint += yAnchorPoint.ToVector2D() * w;

            return new Point2D(interiorPoint.X, interiorPoint.Y);
        }

        private (Point2D xAnchorPoint, Point2D yAnchorPoint) CalculateAnchorPoints(IEnumerable<Point2D> points)
        {
            var xAnchorPoint =
                _novelVertexCalculator.CalculateNovelPointViaNonlinearConstrainedOptimization(new Vector2D(1, 0));
            var yAnchorPoint =
                _novelVertexCalculator.CalculateNovelPointViaNonlinearConstrainedOptimization(new Vector2D(0, 1));

            var tolerance = 0.0001;
            if (xAnchorPoint.X < tolerance) xAnchorPoint = new Point2D(0, xAnchorPoint.Y);
            if (yAnchorPoint.Y < tolerance) yAnchorPoint = new Point2D(yAnchorPoint.X, 0);
            return (xAnchorPoint, yAnchorPoint);
        }
    }
}
