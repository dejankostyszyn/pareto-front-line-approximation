﻿using System;
using System.Linq;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class VertexWeightsManager
    {
        private readonly NormalVectorCalculator _normalVectorCalculator;

        public VertexWeightsManager(NormalVectorCalculator normalVectorCalculator)
        {
            _normalVectorCalculator = normalVectorCalculator;
        }

        public void UpdateVertexWeightsOfLineSegment(VertexWeightsRepository VertexWeightsRepository, LineSegment2D lineSegment)
        {
            throw new NotImplementedException();
        }

        public void InitializeAllVertexWeights(VertexWeightsRepository vertexWeightsRepository, UpperParetoSurface upperParetoSurface, AnchorAndInteriorPointsResult anchorAndInteriorPoints)
        {
            var xAnchorPoint = upperParetoSurface.Facets.First().StartPoint;
            var yAnchorPoint = upperParetoSurface.Facets.Last().EndPoint;
            InitializeAnchorPointWeights(vertexWeightsRepository, xAnchorPoint, yAnchorPoint);
            InitializeNonAnchorPointWeights(vertexWeightsRepository, upperParetoSurface);
        }

        public void InitializeNonAnchorPointWeights(VertexWeightsRepository vertexWeightsRepository, UpperParetoSurface upperParetoSurface)
        {
            var lineSegments = upperParetoSurface.Facets;
            for (int i = 0; i < lineSegments.Length - 1; i++)
            {
                var leftLineSegment = lineSegments[i];
                var rightLineSegment = lineSegments[i + 1];
                var pointBetweenLineSegments = leftLineSegment.EndPoint;
                var weightVector = CalculateWeightVectorAsAverageOfFacetNormals(leftLineSegment, rightLineSegment);
                vertexWeightsRepository.AddOrUpdateWeights(pointBetweenLineSegments, weightVector);
            }
        }

        public void InitializeAnchorPointWeights(VertexWeightsRepository vertexWeightsRepository, Point2D xAnchorPoint, Point2D yAnchorPoint)
        {
            vertexWeightsRepository.AddOrUpdateWeights(xAnchorPoint, new Vector2D(1, 0));
            vertexWeightsRepository.AddOrUpdateWeights(yAnchorPoint, new Vector2D(0, 1));
        }

        private Vector2D CalculateWeightVectorAsAverageOfFacetNormals(LineSegment2D leftLineSegment, LineSegment2D rightLineSegment)
        {
            var leftNormal = _normalVectorCalculator.CalculateFirstNormalVector(leftLineSegment).Normalize();
            var rightNormal = _normalVectorCalculator.CalculateFirstNormalVector(rightLineSegment).Normalize();

            return (leftNormal + rightNormal) * 0.5;
        }
    }
}
