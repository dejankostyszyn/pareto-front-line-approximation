﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;

namespace ParetoFrontLineApproximation
{
    public class VertexWeightsRepository
    {
        private readonly Dictionary<Point2D, Vector2D> _pointToWeightsMapper;
        private readonly HashSet<Vector2D> _markedWeightsMapper;
        private readonly HashSet<LineSegment2D> _lineSegmentMapper;

        public VertexWeightsRepository()
        {
            _pointToWeightsMapper = new Dictionary<Point2D, Vector2D>();
            _markedWeightsMapper = new HashSet<Vector2D>();
            _lineSegmentMapper = new HashSet<LineSegment2D>();
        }

        public bool ContainsPoint(Point2D point) => _pointToWeightsMapper.ContainsKey(point);

        public void AddPointWithWeights(Point2D point, Vector2D weights) => _pointToWeightsMapper.Add(point, weights);

        public void AddOrUpdateWeights(Point2D point, Vector2D weights)
        {
            if (!_pointToWeightsMapper.ContainsKey(point))
                _pointToWeightsMapper.Add(point, weights);
            else
                _pointToWeightsMapper[point] = weights;
        }

        public Vector2D? GetWeightsOfPointOrDefault(Point2D point)
        {
            if (_pointToWeightsMapper.ContainsKey(point)) return _pointToWeightsMapper[point];
            return null;
        }

        public void MarkWeightAsUsed(Vector2D weight) => _markedWeightsMapper.Add(weight);
        public bool HasWeightBeenUsed(Vector2D weight) => _markedWeightsMapper.Contains(weight);
        public void MarkLineSegmentAsUsed(LineSegment2D lineSegment) => _lineSegmentMapper.Add(lineSegment);
        public bool HasLineSegmentBeenUsed(LineSegment2D lineSegment) => _lineSegmentMapper.Contains(lineSegment);

        public void ClearAll()
        {
            _pointToWeightsMapper.Clear();
            _lineSegmentMapper.Clear();
            _markedWeightsMapper.Clear();
        }
    }
}
