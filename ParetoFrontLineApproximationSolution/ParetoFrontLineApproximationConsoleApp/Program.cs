﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using ParetoFrontLineApproximation;
using ParetoFrontLineApproximation.Immutables;
using ScottPlot;

namespace ParetoFrontLineApproximationConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ApproximateParetoFrontLine();
        }

        private static void ApproximateParetoFrontLine()
        {
            var points = CreateRandomPoints(100);

            var vertexWeightsRepository = new VertexWeightsRepository();
            var normalVectorCalculator = new NormalVectorCalculator();
            var lineSegmentsFromPointsCreator = new LineSegmentsFromPointsCreator();
            var novelPointCalculator = new NovelVertexCalculator();
            var vertexValidator = new VertexValidator();
            var hyperplaneBuilder = new HyperplaneBuilder();
            var paretoFrontLineApproximator = new ParetoFrontLineApproximator(
                new AnchorPointsAndInteriorPointCalculator(novelPointCalculator),
                new ConvexHullCalculator(lineSegmentsFromPointsCreator),
                new UpperParetoSurfaceCalculator(normalVectorCalculator),
                new VertexWeightsManager(normalVectorCalculator),
                new LowerDistalPointsCalculator(normalVectorCalculator, hyperplaneBuilder, vertexValidator),
                new FacetSelector(vertexWeightsRepository),
                new NextPointCalculator(normalVectorCalculator, novelPointCalculator, vertexWeightsRepository),
                new BoundingBoxCalculator(hyperplaneBuilder, vertexWeightsRepository),
                vertexValidator,
                new AdditionalSolutionsCalculator(lineSegmentsFromPointsCreator),
                vertexWeightsRepository);

            var paretoFrontLine = paretoFrontLineApproximator.Approximate(points);

            var plot = InitializePlot();
            PlotInputPoints(plot, points);
            PlotParetoFrontLine(plot, paretoFrontLine);
            plot.SetAxisLimits(-0.1, 1.0, -0.1, 1.0);
            plot.Legend(enable: true, location: Alignment.UpperRight);
            plot.SaveFig("Pareto front line.png");
        }

        private static Point2D[] CreateRandomPoints(int numberOfPoints)
        {
            var minValue = 0.1;
            var maxValue = 1.0;
            var randomGenerator = new Random();
            var points = new List<Point2D>();
            for (int i = 0; i < numberOfPoints; i++)
            {
                var angle = randomGenerator.NextDouble() * (Math.PI/4 - minValue) + minValue;
                var value = randomGenerator.NextDouble() * (maxValue - minValue) + minValue;

                var x = Math.Sqrt(value) * Math.Cos(angle) / 2;
                var y = Math.Sqrt(value) * Math.Sin(angle) * 8 / 9;
                points.Add(new Point2D(x, y));
            }

            return points.ToArray();

        }

        private static void PlotParetoFrontLine(Plot plot, ParetoFrontLine paretoFrontLine)
        {
            var points = paretoFrontLine.Vertices;
            var xs = points.Select(x => x.X).ToArray();
            var ys = points.Select(x => x.Y).ToArray();
            plot.AddScatterLines(xs, ys, label: "Pareto front line");
        }

        private static void PlotLowerDistalPoints(Plot plot, LowerDistalPoints lowerDistalPoints)
        {
            var xs = lowerDistalPoints.GetAllLowerDistalPoints().Select(x => x.X).ToArray();
            var ys = lowerDistalPoints.GetAllLowerDistalPoints().Select(x => x.Y).ToArray();
            plot.AddScatterPoints(xs, ys, label: "Lower distal points");
        }

        private static void PlotUpperParetoSurface(Plot plot, UpperParetoSurface upperParetoSurface)
        {
            var xs = upperParetoSurface.Vertices.Select(x => x.X).ToArray();
            var ys = upperParetoSurface.Vertices.Select(x => x.Y).ToArray();
            plot.AddScatterLines(xs, ys, label: "Upper pareto surface");
        }

        private static void PlotConvexHull(Plot plot, ConvexHullResult convexHull)
        {
            var xs = convexHull.Vertices.Select(x => x.X).ToList();
            var ys = convexHull.Vertices.Select(x => x.Y).ToList();
            xs.Add(convexHull.Vertices.First().X);
            ys.Add(convexHull.Vertices.First().Y);
            plot.AddScatterLines(xs.ToArray(), ys.ToArray(), label: "Convex hull");
        }

        private static void PlotInputPoints(Plot plot, Point2D[] points)
        {
            var xs = points.Select(x => x.X).ToArray();
            var ys = points.Select(x => x.Y).ToArray();
            plot.AddScatterPoints(xs, ys, label: "Input points");
        }

        private static void PlotAnchorAndInteriorPoints(Plot plot, AnchorAndInteriorPointsResult anchorInteriorPointsResult)
        {
            var xs = new List<double>();
            var ys = new List<double>();
            xs.AddRange(new []{anchorInteriorPointsResult.XAnchorPoint.X, anchorInteriorPointsResult.YAnchorPoint.X, anchorInteriorPointsResult.InteriorPoint.X});
            ys.AddRange(new []{anchorInteriorPointsResult.XAnchorPoint.Y, anchorInteriorPointsResult.YAnchorPoint.Y, anchorInteriorPointsResult.InteriorPoint.Y});
            plot.AddScatterPoints(xs.ToArray(), ys.ToArray(), label: "Anchor and interior points");
        }

        private static Plot InitializePlot() => new ScottPlot.Plot(800, 600);


    }
}
