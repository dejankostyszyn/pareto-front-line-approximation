﻿using MathNet.Spatial.Euclidean;
using OxyPlot;
using ParetoFrontLineApproximation;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Linq;
using OxyPlot.Series;

namespace ExampleWpf.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly ParetoFrontLineApproximator _paretoFrontLineApproximator;

        private string _title = "Example Pareto Front Line Approximation Plot";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private PlotModel _oxyPlotModel;
        public PlotModel OxyPlotModel
        {
            get { return _oxyPlotModel; }
            set { SetProperty(ref _oxyPlotModel, value); }
        }

        private uint _numberOfPoints;
        public uint NumberOfPoints
        {
            get { return _numberOfPoints; }
            set { SetProperty(ref _numberOfPoints, value); }
        }

        public MainWindowViewModel()
        {
            NumberOfPoints = 100;
            OxyPlotModel = new PlotModel();

            var vertexWeightsRepository = new VertexWeightsRepository();
            var normalVectorCalculator = new NormalVectorCalculator();
            var lineSegmentsFromPointsCreator = new LineSegmentsFromPointsCreator();
            var novelPointCalculator = new NovelVertexCalculator();
            var vertexValidator = new VertexValidator();
            var hyperplaneBuilder = new HyperplaneBuilder();
            _paretoFrontLineApproximator = new ParetoFrontLineApproximator(
                new AnchorPointsAndInteriorPointCalculator(novelPointCalculator),
                new ConvexHullCalculator(lineSegmentsFromPointsCreator),
                new UpperParetoSurfaceCalculator(normalVectorCalculator),
                new VertexWeightsManager(normalVectorCalculator),
                new LowerDistalPointsCalculator(normalVectorCalculator, hyperplaneBuilder, vertexValidator),
                new FacetSelector(vertexWeightsRepository),
                new NextPointCalculator(normalVectorCalculator, novelPointCalculator, vertexWeightsRepository),
                new BoundingBoxCalculator(hyperplaneBuilder, vertexWeightsRepository),
                vertexValidator,
                new AdditionalSolutionsCalculator(lineSegmentsFromPointsCreator),
                vertexWeightsRepository);
        }

        private DelegateCommand _createNewPointsAndFrontLineCommand;

        public DelegateCommand CreateNewPointsAndFrontLineCommand =>
            _createNewPointsAndFrontLineCommand ?? (_createNewPointsAndFrontLineCommand = new DelegateCommand(ExecuteCreateNewPointsAndFrontLineCommand, CanExecuteCreateNewPointsAndFrontLineCommand));

        void ExecuteCreateNewPointsAndFrontLineCommand()
        {
            var points = CreateRandomPoints();
            var paretoFrontLine = _paretoFrontLineApproximator.Approximate(points);
            var vertices = paretoFrontLine.Vertices;
            var scatterPoints = points.Select(x => new ScatterPoint(x.X, x.Y));
            var dataPointsOnLine = vertices.Select(x => new DataPoint(x.X, x.Y));

            var scatterSeries = new ScatterSeries();
            scatterSeries.Points.AddRange(scatterPoints);
            var lineSeries = new LineSeries();
            lineSeries.Points.AddRange(dataPointsOnLine);

            OxyPlotModel.Series.Clear();
            OxyPlotModel.Series.Add(lineSeries);
            OxyPlotModel.Series.Add(scatterSeries);
            OxyPlotModel.InvalidatePlot(true);
        }

        private Point2D[] CreateRandomPoints()
        {
            var minValue = 0.1;
            var maxValue = 1.0;
            var randomGenerator = new Random();
            var points = new Point2D[NumberOfPoints-1];
            for (int i = 0; i < NumberOfPoints - 1; i++)
            {
                var angle = randomGenerator.NextDouble() * (Math.PI / 4 - minValue) + minValue;
                var value = randomGenerator.NextDouble() * (maxValue - minValue) + minValue;

                var x = Math.Sqrt(value) * Math.Cos(angle) / 2;
                var y = Math.Sqrt(value) * Math.Sin(angle) * 8 / 9;

                points[i] = new Point2D(x, y);
            }

            return points;
        }

        bool CanExecuteCreateNewPointsAndFrontLineCommand()
        {
            return true;
        }
    }
}
