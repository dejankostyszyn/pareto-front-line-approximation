﻿using System;
using System.Collections;
using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class BoundingBoxCalculatorTests
    {
        [Test]
        public void Calculate_ThrowArgumentExceptionForEmptyPointsInput_Test()
        {
            var points = new Point2D[]{};
            var boundingBoxCalculator = new BoundingBoxCalculator(new HyperplaneBuilder(),new VertexWeightsRepository());
            var anchorPoints =
                new AnchorAndInteriorPointsResult(new Point2D(0, 1), new Point2D(1, 0), new Point2D(0.5, 0.5));
            Assert.Throws<ArgumentException>(() => boundingBoxCalculator.Calculate(points, new UpperParetoSurface(new Point2D[]{}, new LineSegment2D[]{}), anchorPoints));
        }

        [Test]
        public void Calculate_ThrowArgumentNullExceptionForNullPointsInput_Test()
        {
            Point2D[] points = null;
            var boundingBoxCalculator = new BoundingBoxCalculator(new HyperplaneBuilder(), new VertexWeightsRepository());
            var anchorPoints =
                new AnchorAndInteriorPointsResult(new Point2D(0, 1), new Point2D(1, 0), new Point2D(0.5, 0.5));
            Assert.Throws<ArgumentNullException>(() => boundingBoxCalculator.Calculate(points, new UpperParetoSurface(new Point2D[]{}, new LineSegment2D[]{}), anchorPoints));
        }


        [Test]
        public void Calculate_ThrowArgumentNullExceptionForNullAnchorAndInteriorPointsInput_Test()
        {
            var points = new[]
            {
                new Point2D(0.1, 0.8),
                new Point2D(0.2, 0.4),
                new Point2D(0.6, 0.1),
                new Point2D(0.5, 0.5),
                new Point2D(0.3, 0.1),
                new Point2D(0.8, 0.2),
            };
            var boundingBoxCalculator = new BoundingBoxCalculator(new HyperplaneBuilder(), new VertexWeightsRepository());
            AnchorAndInteriorPointsResult anchorPoints = null;
            Assert.Throws<ArgumentNullException>(() => boundingBoxCalculator.Calculate(points, new UpperParetoSurface(new Point2D[]{}, new LineSegment2D[]{}), anchorPoints));
        }

        [Test]
        public void Calculate_Test()
        {
            var points = new []
            {
                new Point2D(0.0, 0.99),
                new Point2D(0.495, 0.495),
                new Point2D(0.99, 0.0),
            };
            var lineSegmentsCreator = new LineSegmentsFromPointsCreator();
            var normalVectorCalculator = new NormalVectorCalculator();
            var anchorPointsAndInteriorPointCalculator =
                new AnchorPointsAndInteriorPointCalculator(new NovelVertexCalculator());
            var upperParetoSurfaceCalculator = new UpperParetoSurfaceCalculator(normalVectorCalculator);
            var vertexWeightsRepository = new VertexWeightsRepository();
            var vertexWeightsManager = new VertexWeightsManager(normalVectorCalculator);
            var boundingBoxCalculator = new BoundingBoxCalculator(new HyperplaneBuilder(), vertexWeightsRepository);
            var hyperplaneBuilder = new HyperplaneBuilder();
            

            var lineSegments = lineSegmentsCreator.CreateFromConvexHul(points);
            var upperParetoSurface = upperParetoSurfaceCalculator.Calculate(lineSegments);
            var anchorPoints = new AnchorAndInteriorPointsResult(points[0], points.Last(), points[1]);
            vertexWeightsManager.InitializeAllVertexWeights(vertexWeightsRepository, upperParetoSurface, anchorPoints);
            var result = boundingBoxCalculator.Calculate(points, upperParetoSurface, anchorPoints);

            Assert.AreEqual(3, result.LowerBound.Count());
            Assert.AreEqual(0.495, result.UpperXBound);
            Assert.AreEqual(0.495, result.UpperYBound);

            foreach (var lineSegment in upperParetoSurface.Facets)
            {
                var pointOnLine = lineSegment.StartPoint;
                var vertexWeight = vertexWeightsRepository.GetWeightsOfPointOrDefault(pointOnLine).Value;
                var hyperplane = hyperplaneBuilder.Build(vertexWeight, pointOnLine);
                Assert.Contains(hyperplane, (ICollection) result.LowerBound);
            }
        }
    }
}
