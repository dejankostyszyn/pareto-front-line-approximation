﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximationTests{
    [TestFixture]
    public class FacetSelectorTests
    {
        [Test]
        public void GetLowerDistalPointWithHighestDistantToItsLineSegment_Test()
        {
            var facetSelector = new FacetSelector(new VertexWeightsRepository());
            var hyperplaneBuilder = new HyperplaneBuilder();
            var lowerDistalPoints = new LowerDistalPoints(new[]
            {
                new LowerDistalPointResult(new Point2D(0, 0.56666),
                    hyperplaneBuilder.Build(new Vector2D(0.4, 0.1), new Point2D(0, 0.8)),
                    new LineSegment2D(new Point2D(0, 0.8), new Point2D(0.1, 0.4))),
                new LowerDistalPointResult(new Point2D(0.109090909, 0.38484848),
                    hyperplaneBuilder.Build(new Vector2D(0.1, 0.2), new Point2D(0.1, 0.4)),
                    new LineSegment2D(new Point2D(0.1, 0.4), new Point2D(0.3, 0.3))),
                new LowerDistalPointResult(new Point2D(0.975, 0),
                    hyperplaneBuilder.Build(new Vector2D(0.3, 0.7), new Point2D(0.3, 0.3)),
                    new LineSegment2D(new Point2D(0.3, 0.3), new Point2D(1, 0))),
            });

            var result = facetSelector.GetLowerDistalPointWithHighestDistanceToItsLineSegment(lowerDistalPoints, 0.01);

            var expectedResult = lowerDistalPoints.LowerDistalPointResults[0];
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetLowerDistalPointWithHighestDistantToItsLineSegment_ReturnNullIfDistancesAreSmallEnough_Test()
        {
            var facetSelector = new FacetSelector(new VertexWeightsRepository());
            var hyperplaneBuilder = new HyperplaneBuilder();
            var lowerDistalPoints = new LowerDistalPoints(new[]
            {
                new LowerDistalPointResult(new Point2D(0.109090909, 0.38484848),
                    hyperplaneBuilder.Build(new Vector2D(0.1, 0.2), new Point2D(0.1, 0.4)),
                    new LineSegment2D(new Point2D(0.1, 0.4), new Point2D(0.3, 0.3))),
                new LowerDistalPointResult(new Point2D(0.975, 0),
                    hyperplaneBuilder.Build(new Vector2D(0.3, 0.7), new Point2D(0.3, 0.3)),
                    new LineSegment2D(new Point2D(0.3, 0.3), new Point2D(1, 0))),
            });

            var result = facetSelector.GetLowerDistalPointWithHighestDistanceToItsLineSegment(lowerDistalPoints, 0.01);

            Assert.IsNull(result);
        }
    }
}
