﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class VertexWeightsManagerTests
    {
        private AnchorAndInteriorPointsResult _anchorAndInteriorPoints;
        private UpperParetoSurface _upperParetoSurface;

        [SetUp]
        public void SetUp()
        {
            var points = new Point2D[]
            {
                new Point2D(1.0, 0.5),
                new Point2D(1.0, 0.4),
                new Point2D(0.3, 0.3),
                new Point2D(0.35, 0.6),
                new Point2D(0.2, 0.8),
                new Point2D(0.8, 0.7),
                new Point2D(0.6, 0.7),
                new Point2D(0.1, 0.4),
                new Point2D(0.4, 0.5),
            };

            _anchorAndInteriorPoints =
                new AnchorAndInteriorPointsResult(new Point2D(0.5, 0.4), new Point2D(0, 0.8), new Point2D(0.5, 0.4));
            var paretoPoints = new Point2D[]
            {
                new Point2D(0, 0.8), new Point2D(0.1, 0.4), new Point2D(0.3, 0.3), new Point2D(1, 0)
            };

            _upperParetoSurface = new UpperParetoSurface(paretoPoints,
                new LineSegment2D[]
                {
                    new LineSegment2D(new Point2D(0, 0.8), new Point2D(0.1, 0.4)),
                    new LineSegment2D(new Point2D(0.1, 0.4), new Point2D(0.3, 0.3)),
                    new LineSegment2D(new Point2D(0.3, 0.3), new Point2D(1, 0))
                });
        }

        [Test]
        public void InitializeAnchorPointWeights()
        {
            var repository = new VertexWeightsRepository();
            var vertexWeightsManager = new VertexWeightsManager(new NormalVectorCalculator());

            vertexWeightsManager.InitializeAnchorPointWeights(repository, _anchorAndInteriorPoints.XAnchorPoint, _anchorAndInteriorPoints.YAnchorPoint);
            var paretoPoints = _upperParetoSurface.Vertices;

            Assert.False(repository.ContainsPoint(paretoPoints[1]));
            Assert.AreEqual(new Vector2D(1, 0), repository.GetWeightsOfPointOrDefault(_anchorAndInteriorPoints.XAnchorPoint));
            Assert.AreEqual(new Vector2D(0, 1), repository.GetWeightsOfPointOrDefault(_anchorAndInteriorPoints.YAnchorPoint));
        }

        [Test]
        public void InitializeNonAnchorPointWeights()
        {
            var repository = new VertexWeightsRepository();
            var vertexWeightsManager = new VertexWeightsManager(new NormalVectorCalculator());

            vertexWeightsManager.InitializeNonAnchorPointWeights(repository, _upperParetoSurface);
            var paretoPoints = _upperParetoSurface.Vertices;

            Assert.False(repository.ContainsPoint(_anchorAndInteriorPoints.XAnchorPoint));
            Assert.False(repository.ContainsPoint(_anchorAndInteriorPoints.YAnchorPoint));

            var result = repository.GetWeightsOfPointOrDefault(paretoPoints[1]);
            var expectedResult = new Vector2D(0.7, 0.56);

            Assert.AreEqual(expectedResult.X, result.Value.X, 0.01);
            Assert.AreEqual(expectedResult.Y, result.Value.Y, 0.01);
        }
    }
}
