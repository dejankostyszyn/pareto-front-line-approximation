﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class NormalVectorCalculatorTests
    {
        [TestCase(0.1, 0.2, 0.3, 0.4, -0.2, 0.2)]
        [TestCase(0.0, 0.2, 0.3, 0.4, -0.2, 0.3)]
        [TestCase(0.0, 0.2, 0.3, 0.0, 0.2, 0.3)]
        public void CalculateFirstNormalVector(double point1_1, double point1_2, double point2_1, double point2_2,
            double resultVector_1, double resultVector_2)
        {
            var lineSegment = new LineSegment2D(new Point2D(point1_1, point1_2), new Point2D(point2_1, point2_2));
            var expectedResultFirstNormalVector = new Vector2D(resultVector_1, resultVector_2);

            var normalVectorCalculator = new NormalVectorCalculator();
            var resultFirstNormalVector = normalVectorCalculator.CalculateFirstNormalVector(lineSegment);

            Assert.AreEqual(expectedResultFirstNormalVector.X, resultFirstNormalVector.X, 1E-4);
            Assert.AreEqual(expectedResultFirstNormalVector.Y, resultFirstNormalVector.Y, 1E-4);
        }

        [TestCase(0.1, 0.2, 0.3, 0.4, 0.2, -0.2)]
        [TestCase(0.0, 0.2, 0.3, 0.4, 0.2, -0.3)]
        [TestCase(0.0, 0.2, 0.3, 0.0, -0.2, -0.3)]
        public void CalculateSecondNormalVector(double point1_1, double point1_2, double point2_1, double point2_2,
            double resultVector_1, double resultVector_2)
        {
            var lineSegment = new LineSegment2D(new Point2D(point1_1, point1_2), new Point2D(point2_1, point2_2));
            var expectedResultSecondNormalVector = new Vector2D(resultVector_1, resultVector_2);

            var normalVectorCalculator = new NormalVectorCalculator();
            var resultSecondNormalVector = normalVectorCalculator.CalculateSecondNormalVector(lineSegment);

            Assert.AreEqual(expectedResultSecondNormalVector.X, resultSecondNormalVector.X, 1E-4);
            Assert.AreEqual(expectedResultSecondNormalVector.Y, resultSecondNormalVector.Y, 1E-4);
        }
    }
}
