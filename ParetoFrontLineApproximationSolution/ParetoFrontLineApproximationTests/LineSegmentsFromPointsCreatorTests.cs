﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class LineSegmentsFromPointsCreatorTests
    {
        [Test]
        public void CreateFromOrderedDescendingWrtX_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.6),
                new Point2D(0.3, 0.5),
                new Point2D(0.7, 0.1),
            };

            var lineSegmentsFromPointsCreator = new LineSegmentsFromPointsCreator();
            var result = lineSegmentsFromPointsCreator.CreateFromOrderedDescendingWrtX(points);

            var expectedResult = new LineSegment2D[]
            {
                new LineSegment2D(points[0], points[1]),
                new LineSegment2D(points[1], points[2]),
            };

            Assert.AreEqual(expectedResult, result);
        }
        [Test]
        public void CreateFromConvexHull_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.6),
                new Point2D(0.3, 0.5),
                new Point2D(0.7, 0.1),
            };

            var lineSegmentsFromPointsCreator = new LineSegmentsFromPointsCreator();
            var result = lineSegmentsFromPointsCreator.CreateFromConvexHul(points);

            var expectedResult = new LineSegment2D[]
            {
                new LineSegment2D(points[2], points[0]),
                new LineSegment2D(points[0], points[1]),
                new LineSegment2D(points[1], points[2]),
            };

            Assert.AreEqual(expectedResult, result);
        }


    }
}
