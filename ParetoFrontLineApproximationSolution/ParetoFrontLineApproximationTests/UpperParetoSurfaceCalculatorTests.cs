﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class UpperParetoSurfaceCalculatorTests
    {
        [Test]
        public void Calculate_Test()
        {
            var upperParetoSurfaceCalculator = new UpperParetoSurfaceCalculator(new NormalVectorCalculator());

            var points = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.5, 0.1),
                new Point2D(0.5, 0.8),
                new Point2D(0.8, 0.5),
                new Point2D(0.9, 0.1),
            };

            var lineSegments = new LineSegment2D[]
            {
                new LineSegment2D(points[0], points[1]),
                new LineSegment2D(points[1], points[2]),
                new LineSegment2D(points[2], points[5]),
                new LineSegment2D(points[5], points[4]),
                new LineSegment2D(points[4], points[3]),
                new LineSegment2D(points[3], points[0]),
            };

            var expectedPoints = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.5, 0.1),
                new Point2D(0.9, 0.1),
            };

            var expectedLineSegments = new LineSegment2D[]
            {
                new LineSegment2D(expectedPoints[0], expectedPoints[1]),
                new LineSegment2D(expectedPoints[1], expectedPoints[2]),
                new LineSegment2D(expectedPoints[2], expectedPoints[3]),
            };

            var expectedResult = new UpperParetoSurface(expectedPoints, expectedLineSegments);

            var result = upperParetoSurfaceCalculator.Calculate(lineSegments);

            Assert.AreEqual(expectedResult.Vertices.Length, result.Vertices.Length);
            Assert.AreEqual(expectedResult.Facets.Length, result.Facets.Length);

            foreach (var expectedPoint in expectedPoints) Assert.Contains(expectedPoint, expectedResult.Vertices);
            foreach (var expectedLineSegment in expectedLineSegments)
                Assert.Contains(expectedLineSegment, expectedResult.Facets);
        }

        [Test]
        public void Calculate_ThrowArgumentExceptionIfLessThanOneLineSegmentAsInput_Test()
        {
            var lineSegments = new LineSegment2D[] { };
            var upperParetoSurfaceCalculator = new UpperParetoSurfaceCalculator(new NormalVectorCalculator());

            Assert.Throws<ArgumentException>(() => upperParetoSurfaceCalculator.Calculate(lineSegments));
        }

        [Test]
        public void Calculate_OneLineSegmentAsInput_Test()
        {
            var lineSegments = new LineSegment2D[] { new LineSegment2D(new Point2D(0.2, 0.3), new Point2D(0.5, 0.1)) };
            var upperParetoSurfaceCalculator = new UpperParetoSurfaceCalculator(new NormalVectorCalculator());
            var result = upperParetoSurfaceCalculator.Calculate(lineSegments);

            Assert.AreEqual(2, result.Vertices.Length);
            Assert.AreEqual(1, result.Facets.Length);
            Assert.Contains(lineSegments[0].StartPoint, result.Vertices);
            Assert.Contains(lineSegments[0].EndPoint, result.Vertices);
            Assert.Contains(lineSegments[0], result.Facets);
        }
    }
}
