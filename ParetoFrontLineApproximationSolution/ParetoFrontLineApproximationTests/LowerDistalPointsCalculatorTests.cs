﻿using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class LowerDistalPointsCalculatorTests
    {
        [Test]
        public void Calculate_Test()
        {
            var vertices = new Point2D[]
            {
                new Point2D(0, 0.8),
                new Point2D(0.1, 0.4),
                new Point2D(0.3, 0.3),
                new Point2D(1, 0)
            };

            var facets = new LineSegment2D[]
            {
                new LineSegment2D(vertices[0], vertices[1]),
                new LineSegment2D(vertices[1], vertices[2]),
                new LineSegment2D(vertices[2], vertices[3]),
            };

            var expectedPoints = new Point2D[]
            {
                new Point2D(0.95, 0),
            };

            var normalVectorCalculator = new NormalVectorCalculator();
            var vertexWeightsRepository = new VertexWeightsRepository();
            var vertexWeightsManager = new VertexWeightsManager(normalVectorCalculator);
            var anchorPointsAndInteriorPointCalculator =
                new AnchorPointsAndInteriorPointCalculator(new NovelVertexCalculator());
            var anchorAndInteriorPoints =
                new AnchorAndInteriorPointsResult(vertices[0], vertices.Last(), new Point2D(0.5, 0.4));
            var upperParetoSurface = new UpperParetoSurface(vertices, facets);
            vertexWeightsManager.InitializeAllVertexWeights(vertexWeightsRepository, upperParetoSurface, anchorAndInteriorPoints);
            var lowerDistalPointsCalculator =
                new LowerDistalPointsCalculator(normalVectorCalculator, new HyperplaneBuilder(), new VertexValidator());

            var boundingBoxCalculator = new BoundingBoxCalculator(new HyperplaneBuilder(), vertexWeightsRepository);
            var boundingBox = boundingBoxCalculator.Calculate(vertices, new UpperParetoSurface(vertices, facets),
                anchorAndInteriorPoints);
            var result = lowerDistalPointsCalculator.Calculate(vertexWeightsRepository, facets, boundingBox);

            Assert.AreEqual(expectedPoints.Length, result.LowerDistalPointResults.Length);

            for (var i = 0; i < expectedPoints.Length; i++)
            {
                var expectedPoint = expectedPoints[i];
                var resultPoint = result.LowerDistalPointResults[i].LowerDistalPoint;
                Assert.AreEqual(expectedPoint.X, resultPoint.X, 1E-2);
                Assert.AreEqual(expectedPoint.Y, resultPoint.Y, 1E-2);
            }
        }
    }
}
