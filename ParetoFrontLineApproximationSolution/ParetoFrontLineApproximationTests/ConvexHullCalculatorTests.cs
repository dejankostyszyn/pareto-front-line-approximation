﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class ConvexHullCalculatorTests
    {
        [Test]
        public void Calculate()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.4, 0.4),
                new Point2D(0.4, 0.6),
                new Point2D(0.5, 0.1),
                new Point2D(0.5, 0.8),
                new Point2D(0.6, 0.3),
                new Point2D(0.8, 0.5),
                new Point2D(0.9, 0.1),
            };

            var expectedPoints = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.5, 0.1),
                new Point2D(0.5, 0.8),
                new Point2D(0.8, 0.5),
                new Point2D(0.9, 0.1),
            };
            var expectedLineSegments = new LineSegment2D[]
            {
                new LineSegment2D(expectedPoints[3], expectedPoints[0]),
                new LineSegment2D(expectedPoints[0], expectedPoints[1]),
                new LineSegment2D(expectedPoints[1], expectedPoints[2]),
                new LineSegment2D(expectedPoints[2], expectedPoints[5]),
                new LineSegment2D(expectedPoints[5], expectedPoints[4]),
                new LineSegment2D(expectedPoints[4], expectedPoints[3]),
            };

            var convexHullCalculator = new ConvexHullCalculator(new LineSegmentsFromPointsCreator());
            var result = convexHullCalculator.Calculate(points);

            Assert.AreEqual(expectedPoints.Length, result.Vertices.Length);
            foreach (var expectedPoint in expectedPoints) Assert.Contains(expectedPoint, result.Vertices);

            Assert.AreEqual(expectedLineSegments.Length, result.Facets.Length);
            foreach (var expectedLineSegment in expectedLineSegments)
                Assert.Contains(expectedLineSegment, result.Facets);
        }
        [Test]
        public void Calculate_ThrowArgumentExceptionIfLessThanTwoPointsAsArgument_Test()
        {
            var points = new Point2D[] { new Point2D(0.5, 0.5) };
            var convexHullCalculator = new ConvexHullCalculator(new LineSegmentsFromPointsCreator());

            Assert.Throws<ArgumentException>(() => convexHullCalculator.Calculate(points));

        }

        [Test]
        public void Calculate_ThreePointsOnOneLineAsInput_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.5),
                new Point2D(0.3, 0.5),
                new Point2D(0.5, 0.5),
            };

            var expectedLineSegments = new LineSegment2D[]
            {
                new LineSegment2D(points[2], points[0]),
                new LineSegment2D(points[0], points[2]),
            };

            var convexHullCalculator = new ConvexHullCalculator(new LineSegmentsFromPointsCreator());
            var result = convexHullCalculator.Calculate(points);

            Assert.AreEqual(expectedLineSegments.Length, result.Facets.Length);
            foreach (var expectedLineSegment in expectedLineSegments)
                Assert.Contains(expectedLineSegment, result.Facets);
        }

        [Test]
        public void Calculate_TwoPointsAsInput_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.5),
                new Point2D(0.5, 0.5),
            };

            var expectedLineSegments = new LineSegment2D[]
            {
                new LineSegment2D(points[1], points[0]),
                new LineSegment2D(points[0], points[1]),
            };

            var convexHullCalculator = new ConvexHullCalculator(new LineSegmentsFromPointsCreator());
            var result = convexHullCalculator.Calculate(points);

            Assert.AreEqual(expectedLineSegments.Length, result.Facets.Length);
            foreach (var expectedLineSegment in expectedLineSegments)
                Assert.Contains(expectedLineSegment, result.Facets);
        }
    }
}
