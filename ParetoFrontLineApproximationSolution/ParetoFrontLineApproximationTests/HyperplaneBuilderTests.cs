﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class HyperplaneBuilderTests
    {
        [Test]
        public void Build()
        {
            var normalVector = new Vector2D(0.3, 0.7);
            var pointOnHyperplane = new Point2D(0.5, 0.6);
            var hyperPlaneBuilder = new HyperplaneBuilder();
            var result = hyperPlaneBuilder.Build(normalVector, pointOnHyperplane);
            var expectedResult = new Hyperplane(normalVector, pointOnHyperplane, 0.56999);

            Assert.AreEqual(expectedResult.C, result.C, 1E-3);
            Assert.AreEqual(expectedResult.NormalVector.X, result.NormalVector.X, 1E-3);
            Assert.AreEqual(expectedResult.NormalVector.Y, result.NormalVector.Y, 1E-3);
            Assert.AreEqual(expectedResult.PointOnPlane.X, result.PointOnPlane.X, 1E-3);
            Assert.AreEqual(expectedResult.PointOnPlane.Y, result.PointOnPlane.Y, 1E-3);
        }
    }
}
