﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class AnchorPointsAndInteriorPointCalculatorTests
    {
        [Test]
        public void Calculate_ThrowArgumentExceptionForEmptyArgumentEnumerable_Test()
        {
            var points = new Point2D[] { };
            var calculator = new AnchorPointsAndInteriorPointCalculator(new NovelVertexCalculator());
            Assert.Throws<ArgumentException>(() => calculator.Calculate(points));
        }
    }
}
