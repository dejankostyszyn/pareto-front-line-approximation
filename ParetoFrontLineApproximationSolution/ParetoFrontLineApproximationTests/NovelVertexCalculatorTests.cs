﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class NovelVertexCalculatorTests
    {
        [Test]
        public void CalculateNovelPointViaNonlinearConstrainedOptimization_StressTest()
        {
            var novelVertexCalculator = new NovelVertexCalculator();
            var random = new Random();
            for (int i = 0; i < 10 * 1000; i++)
            {
                var x = random.NextDouble();
                var y = 1.0 - x;
                var weightVector = new Vector2D(x, y);
                Assert.NotNull(novelVertexCalculator.CalculateNovelPointViaNonlinearConstrainedOptimization(weightVector));
            }
        }

    }
}
