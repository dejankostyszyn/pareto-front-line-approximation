﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoFrontLineApproximation;

namespace ParetoFrontLineApproximationTests
{
    [TestFixture]
    public class VertexWeightsRepositoryTests
    {
        private Point2D _point1;
        private Point2D _point2;
        private Vector2D _weight1;
        private Vector2D _weight2;

        [SetUp]
        public void SetUp()
        {
            _point1 = new Point2D(0.5, 1.0);
            _point2 = new Point2D(0.2, 0.3);

            _weight1 = new Vector2D(1, 0);
            _weight2 = new Vector2D(0, 1);
        }

        [Test]
        public void AddPointWithWeights_Test()
        {
            var repository = new VertexWeightsRepository();
            Assert.False(repository.ContainsPoint(_point1));
            repository.AddPointWithWeights(_point1, _weight1);
            Assert.True(repository.ContainsPoint(_point1));
        }

        [Test]
        public void AddPointsWithWeights_ThrowsArgumentExceptionIfPointAlreadyExists_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.AddPointWithWeights(_point1, _weight1);
            Assert.Throws<ArgumentException>(() => repository.AddPointWithWeights(_point1, _weight1));
        }

        [Test]
        public void AddPointWithWeights_AddNewPoint_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.AddOrUpdateWeights(_point1, _weight1);
            Assert.AreEqual(_weight1, repository.GetWeightsOfPointOrDefault(_point1));
        }

        [Test]
        public void AddPointWithWeights_UpdatePointWeights_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.AddOrUpdateWeights(_point1, _weight1);
            repository.AddOrUpdateWeights(_point1, _weight2);
            Assert.AreEqual(_weight2, repository.GetWeightsOfPointOrDefault(_point1));
        }

        [Test]
        public void GetWeightsOfPointOrDefault_ReturnWeightWhenExists_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.AddOrUpdateWeights(_point1, _weight1);
            Assert.AreEqual(_weight1, repository.GetWeightsOfPointOrDefault(_point1));
        }

        [Test]
        public void GetWeightsOfPointOrDefault_ReturnNullWhenWeightDoesNotExist_Test()
        {
            var repository = new VertexWeightsRepository();
            Assert.IsNull(repository.GetWeightsOfPointOrDefault(_point1));
        }

        [Test]
        public void MarkWeightAsUsed_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.MarkWeightAsUsed(_weight1);
            repository.MarkWeightAsUsed(_weight2);

            Assert.IsTrue(repository.HasWeightBeenUsed(_weight1));
            Assert.IsTrue(repository.HasWeightBeenUsed(_weight2));
        }

        [Test]
        public void HasWeightBeenUsed_ReturnFalseIfPointExistsButWeightHasNotWeenUsed_Test()
        {
            var repository = new VertexWeightsRepository();
            repository.MarkWeightAsUsed(_weight1);

            Assert.IsFalse(repository.HasWeightBeenUsed(_weight2));
        }

        [Test]
        public void HasWeightBeenUsed_ReturnFalseIfPointHasNoMarkedWeights_Test()
        {
            var repository = new VertexWeightsRepository();

            Assert.IsFalse(repository.HasWeightBeenUsed(_weight2));
        }
    }
}
