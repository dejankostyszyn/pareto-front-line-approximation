# Pareto Front Line Approximation

A C# .Net Framework 4.7.2 implementation of the proposed algorithm for 2D Pareto front line approximation from the scientific paper:

Craft, D.L., Halabi, T.F., Shih, H.A. and Bortfeld, T.R. (2006), Approximating convex Pareto surfaces in multiobjective radiotherapy planning. Med. Phys., 33: 3399-3407. https://doi.org/10.1118/1.2335486

An example how to use it is proposed as console application at path pareto-front-line-approximation/ParetoFrontLineApproximationSolution/ParetoFrontLineApproximationConsoleApp/Program.cs
